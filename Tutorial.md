# Terraform
Write a TF file to spin up an EC2 instance. There are a bunch of tutorials out there.
 * Once you're creating a node log into it and throw a little hello world website on it. You'll automate that later.
 * Update it to create a target group
 * Update it to create a load balancer in front of it.
 * Update it so that all of your objects are named something like my-project-prod

# Ansible
 * Write an ansible whatever (script?) that installs your hello world website and starts it.
 * Update the ansible script to uninstall some of the default things in the image which could be security vulnerabilities.
 * Update it to download and install some free antivirus software or anything that will take multiple steps to install.

# Terraform
 * Update your TF so that it is parameterized. Take in an "environment" variable and replace "prod" in your object names with that variable.
 * Now provide that var when you run TF next.
 * Next turn the bits that spin up your node(s) and your LB into terraform modules. This is a tiny bit non-obvious but once you figure it out it turns out to be pretty simple.

# Docker
 * Build your app into a docker container.

# Terraform
 * Update your TF to create an S3 bucket. Make sure it has a parameterized environment portion of the name.
 * Update it to store it's state file in an S3 bucket. Just create the buckets by hand one for each environment. [This looks like a decent tutorial](https://blog.gruntwork.io/how-to-manage-terraform-state-28f5697e68fa)
 * Make sure to parameterize the bucket name to include environment.

# Ansible
 * Update your ansible script so that it no longer installs your web application.
 * Now have it install docker. Docker may already be installed. If so it might be an old really awkward installation. Have your script uninstall the existing docker and install a current one.
 * Then have ansible pull down your container and start it.

# Docker
 * Update your container so that it takes an argument and sets an environment variable. Have your app's hello world say "Hello <contents of said variable>"

# Circle-CI
 * Write a pipeline that runs your terraform when you push to any branch.
 * Delete your docker container and update your pipeline to build the container fresh and then upload it to s3.
 * Update your pipeline so that on a merge to master it deploys to non-prod.
 * Update the pipeline so that it deploys to non-prod. Then waits for a manual action to continue. On continue it publishes to prod.
 * I've never used circle-ci so this part may or may not be an option.
 * Update it so that the manual action can be circumvented based on an environment variable set in your pipeline file. If AUTO_PUBLISH_PROD is == "true" we skip the manual step.

# Bonus points
 * Update your docker container to do a multi-stage build.
 * In the first stage delete a bunch of un-neccessary packages.
 * Then in the second stage build from "scratch" and just copy / from stage one to / of stage two.
 * The result is a much smaller container.
 * Created two load balancers.
 * The EC2 LB will now be a back end API.
 * Create the front end as a lambda and have it make a call to your other LB.

# NOTES/CAVEATS
 * Make sure and delete your garbage so that you're not getting billed for a ton of AWS shit.
 * I would run terraform destroy before you make changes to your TF files.
